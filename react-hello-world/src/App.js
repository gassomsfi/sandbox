
    import React, {Component} from 'react';
    import Weather from './components/weather'

    class App extends Component {

      state = { weather: []}

      componentDidMount(){
        fetch('https://localhost:5001/WeatherForecast')
        .then(res => res.json())
        .then((data) => {
          this.setState({ weather: data})
        })
        .catch(console.log)
      }

      render () {
        return (
          <Weather weather={this.state.weather} />
        )
      }
    }

    export default App;