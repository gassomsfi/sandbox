import React from 'react'

const Weather = ({ weather }) => {
  return (
    <div>
      <center><h1>Weather List</h1></center>
      {weather.map((forcast) => (
        <div class="card">
          <div class="card-body">
            <h5 class="card-title">{forcast.date}</h5>
            <h6 class="card-subtitle mb-2 text-muted">{forcast.temperatureF}</h6>
            <p class="card-text">{forcast.summary}</p>
          </div>
        </div>
      ))}
    </div>
  )
};

export default Weather